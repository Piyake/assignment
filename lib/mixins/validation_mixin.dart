import 'package:flutter/foundation.dart';

class ValidateMixin{
    mandatoryValidation(context,{@required dynamic value,String? currentError}){
        return  mandatoryValidationEx(context,value:value,customMessge: null,currentError: currentError);
    }

    mandatoryValidationEx(context,{required dynamic value, String? customMessge,String? currentError}){
        if(currentError!=null) return currentError;
        bool isEmpty = false;
        if(value==null){
            isEmpty=true;
        }else if (value is String && value==''){
            isEmpty=true;
        }else if(value is List && value.isEmpty){
            isEmpty=true;
        }
        
        if(isEmpty){
            return customMessge==null?'opps you fogot to fill this':customMessge; 
        }
    }
}