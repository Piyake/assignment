import 'package:flutter/material.dart';

class DecoMixin{
    profileIconDecoration(){
        return BoxDecoration(
            shape: BoxShape.circle,
        );
    }

    menuDecoration(context){
        return BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
                        'assets/logos/logoa.png',
                    ),
                    fit: BoxFit.contain,
                    alignment: Alignment.bottomRight,
                    scale: 3,
                    colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.7), BlendMode.dstOut)
                ),
                gradient: LinearGradient(
                    colors: [Color(0xFF0066FF), Colors.blue],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter),
            );
    }

    static appBarShape(){
        return RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(25),
            )
        );
    }

    EdgeInsets cardMargin(){
        return EdgeInsets.only(bottom: 6);
    }

    EdgeInsets cardPadding(){
        return EdgeInsets.symmetric(vertical:8,horizontal:10);
    }

    cardDeco(context,{@required isActive,double radius=13,Color? color}){
        return BoxDecoration(
            color: color??Theme.of(context).cardColor,   
            borderRadius: BorderRadius.all(Radius.circular(radius)),
            boxShadow: [
                BoxShadow(
                    color: isActive?Colors.black.withOpacity(0.6):Colors.black.withOpacity(0.2),
                    spreadRadius:1,
                    blurRadius: 2,
                    offset: Offset(0,2),
                )
            ],
        );
    }
    
    searchInputDeco(context,{required String placeholder}){
        return InputDecoration(
            icon:  Icon(Icons.search,size: 20,),
            border: InputBorder.none,
            helperMaxLines: 1,
            hintMaxLines: 1,
            hintText: placeholder,
            contentPadding: EdgeInsets.all(0),
            hintStyle: Theme.of(context).textTheme.bodyText2!.copyWith(
                height: .5
            ),
            
        );
    }

    boxShadowDeco(){
        return [
            BoxShadow(
                color: Colors.black54,
                blurRadius: 15.0,
                offset: Offset(0.0, 0.75)
            )
        ];
    }

    inputBorder(context,String? errorText){
        if(errorText==null){
            return OutlineInputBorder(borderSide: BorderSide(),borderRadius: BorderRadius.circular(13));
        }else{
            return OutlineInputBorder(borderSide: BorderSide(color: Colors.red,width: 2),borderRadius: BorderRadius.only(topLeft:Radius.circular(13),topRight:Radius.circular(13)));
        }
    }
    
    inputBorderColor(context,String? errorText){
        if(errorText==null){
            return Theme.of(context).textTheme.bodyText1!.color;
        }else{
            return Colors.red;
        }
    }
    
    inputBorderRadius(context,String? errorText){
        if(errorText==null){
            return BorderRadius.all(Radius.circular(13));
        }else{
            return BorderRadius.only(topLeft:Radius.circular(13),topRight:Radius.circular(13));
        }
    }

}