
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PopMessage{
    static bool isPopupOpen = false;
    static success(String message)async{
        Fluttertoast.showToast(
            msg: message,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 12.0,
        );
    }

    static error(String message)async{
        Fluttertoast.showToast(
            msg: message,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 12.0,
        );
    }

    static info(String message)async{
        Fluttertoast.showToast(
            msg: message,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.white,
            textColor: Colors.blue[700],
            fontSize: 12.0,
        );
    }

    static warn(String message)async{
        Fluttertoast.showToast(
            msg: message,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.amber[200],
            textColor: Colors.black,
            fontSize: 12.0,
        );
    }

    static exception(e){
        PopMessage.error(e.message);
    }
}