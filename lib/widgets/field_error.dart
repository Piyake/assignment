import 'package:flutter/material.dart';

class FieldError extends StatelessWidget {
    final String? message;
    FieldError({required this.message});

    @override
    Widget build(BuildContext context) {
        return message==null?SizedBox():errorWidget(context);            
    }

    Widget errorWidget(context){
        return Container(
            key: key,
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(13),
                    bottomRight: Radius.circular(13)
                )
            ),
            padding: EdgeInsets.symmetric(vertical: 3,horizontal: 8),
            child:Text(
                message!,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(color:Colors.white),
            )
        );
    }
}