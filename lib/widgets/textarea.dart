import 'package:assignment/mixins/deco_mixin.dart';
import 'package:flutter/material.dart';

import 'field_error.dart';

class TextArea extends StatefulWidget {
    final placeholder;
    final String? errorText;
    final TextEditingController controller;
    final bool isEditable;
    final Function(String)? onChanged; 
    
    TextArea({
        required this.controller,
        required this.placeholder,
        this.errorText,
        this.isEditable=true,
        this.onChanged
    });
    
    @override
    _TextAreaState createState() => _TextAreaState();
}

class _TextAreaState extends State<TextArea> with DecoMixin{
    @override
    void initState() {
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        OutlineInputBorder bd = inputBorder(context,widget.errorText);
        return Container(
            padding: EdgeInsets.only(top: 4,bottom: 6),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children:[
                    Container(
                        child: TextField(
                            textInputAction: TextInputAction.go,
                            enabled: widget.isEditable,
                            decoration: InputDecoration(
                                fillColor: Theme.of(context).backgroundColor,
                                filled: true,
                                contentPadding: EdgeInsets.symmetric(horizontal:10,vertical: 4),
                                border:bd,
                                focusedBorder:bd,
                                disabledBorder: bd,
                                enabledBorder:bd,
                                hintText: widget.placeholder
                            ),
                            style: Theme.of(context).textTheme.bodyText1,
                            maxLines: 6,
                            minLines: 6,
                            controller: widget.controller,
                            onChanged: widget.onChanged
                        ),
                    ),
                    FieldError(message: widget.errorText)
                ]
            ),
        );
    }
}