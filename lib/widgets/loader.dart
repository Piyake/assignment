import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loader{
    static Widget pageLoad(){
        return Builder(
            builder: (BuildContext context){
                return Container(
                    child:SpinKitThreeBounce(
                        color: Colors.blue,
                        size:18,
                    )
                );
            }
        );
    }
   
    static Widget cardLoad(){
        return Builder(
            builder: (BuildContext context){
                return Container(
                    decoration: BoxDecoration(
                        color:Theme.of(context).canvasColor.withOpacity(0.7),
                    ),
                    child:SpinKitThreeBounce(
                        color: Colors.blue,
                        size:18,
                    )
                );
            }
        );
    }
}