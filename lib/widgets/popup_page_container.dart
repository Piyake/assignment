import 'package:flutter/material.dart';

import 'loader.dart';

class PopupPageContainer extends StatelessWidget {
    final String title;
    final Widget child;
    final List<Widget> actions;
    final bool loading;

    const PopupPageContainer({required this.title,required this.child,required this.actions, this.loading=false});     

    @override
    Widget build(BuildContext context) {
        return AlertDialog(
            insetPadding: EdgeInsets.all(10),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            titlePadding: EdgeInsets.symmetric(horizontal: 20,vertical:20),
            title: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Expanded(
                        child: Text(
                            title,
                            style: Theme.of(context).textTheme.headline6,
                        ),
                    ),
                    InkWell(child:Icon(Icons.close),onTap: (){Navigator.of(context).pop();}, )
                ],
            ),
            scrollable: true,
            contentPadding:EdgeInsets.zero,
            actionsPadding: EdgeInsets.zero,
            content: Container(
                width:500,
                child: Stack(
                    children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                              children: [
                                  SizedBox(height: 12),
                                  child,
                                  Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 12.0),
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: actions,
                                    ),
                                  )
                              ],
                          ),
                        ),
                        if(loading)Positioned(bottom: 0,top:0,left:0,right:0,child: Loader.cardLoad())
                    ]
                ),
            )
        );
    }
}