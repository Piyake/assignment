import 'package:assignment/mixins/deco_mixin.dart';
import 'package:assignment/widgets/field_error.dart';
import 'package:flutter/material.dart';

class TextBox extends StatefulWidget {
    final placeholder;
    final String? errorText;
    final TextEditingController controller;
    final bool isEditable;
    final TextInputType? keyBoard;
    final Function(String)? onChanged; 
    final EdgeInsets contentPadding; 
    
    TextBox({
        required this.controller,
        required this.placeholder,
        this.errorText,
        this.isEditable=true,
        this.onChanged,
        this.keyBoard,
        this.contentPadding=const EdgeInsets.symmetric(horizontal:10)
    });
    
    @override
    _TextBoxState createState() => _TextBoxState();
}

class _TextBoxState extends State<TextBox> with DecoMixin{
    @override
    void initState() {
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        OutlineInputBorder bd = inputBorder(context,widget.errorText);
        return Container(
            padding: EdgeInsets.only(top: 4,bottom: 6),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children:[
                    Container(
                        height: 40,
                        child: TextField(
                            keyboardType:widget.keyBoard,
                            enabled: widget.isEditable,
                            decoration: InputDecoration(
                                fillColor: Theme.of(context).backgroundColor,
                                filled: true,
                                contentPadding: widget.contentPadding,
                                border:bd,
                                focusedBorder:bd,
                                enabledBorder:bd,
                                disabledBorder: bd,
                                hintText: widget.placeholder
                            ),
                            style: Theme.of(context).textTheme.bodyText2,
                            maxLines: 1,
                            minLines: 1,
                            controller: widget.controller,
                            onChanged: widget.onChanged,
                        ),
                    ),
                    FieldError(message: widget.errorText)
                ]
            ),
        );
    }
}