import 'dart:ui';

import 'package:flutter/material.dart';

class FieldLabel extends StatelessWidget {
    final String label;
    final bool isMandatory;
    FieldLabel({required this.label,this.isMandatory=false});
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top:6),
        child:Row(
            children:[
                Text(
                    label,
                    style: Theme.of(context).textTheme.bodyText2,
                ),
                SizedBox(width:3),
                isMandatory
                    ?Text(
                        '*',
                        style: TextStyle(
                            fontFeatures: [
                                FontFeature.enable('sups')
                            ],
                            color: Colors.red,
                            fontWeight: FontWeight.w600
                        ),
                    )
                    :SizedBox(),
            ],
        )
    );
  }
}