
import 'package:assignment/routes/route_constants.dart';
import 'package:assignment/views/common_views/not_found_view.dart';
import 'package:assignment/views/home_view.dart';
import 'package:assignment/views/splash_view.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings){
    switch (settings.name) {
        case AppRoutes.SplashScreen:
            return MaterialPageRoute(builder: (context)=>SplashView(),settings: settings);
        case AppRoutes.HomeView:
            return MaterialPageRoute(builder: (context)=>HomeView(),settings: settings);
        default :
            return MaterialPageRoute(builder: (context)=>UnKnownRoute(name:settings.name??""),settings: settings);
    }               
}