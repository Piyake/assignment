import 'dart:async';

import 'package:assignment/routes/route_constants.dart';
import 'package:assignment/services/screenshot_manager.dart';
import 'package:assignment/services/time_change_manager.dart';
import 'package:assignment/utils/session.dart';
import 'package:assignment/widgets/pop_message.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:time_change_detector/time_change_detector.dart';
import 'routes/router.dart' as router;

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
    @override
    _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>{ 
    Stream<bool>? _controller;
    late StreamSubscription _subscription;
    @override
    void initState() {
        super.initState();
        _initWatcher();
        Connectivity().onConnectivityChanged.listen((ConnectivityResult result) async{
            try{
                await Session.updateInternetStatus(result);
                Duration d = DateTime.now().difference(DateTime.parse(Session.capturedDate!));
                if(Session.hasInternet){
                    if(Session.fileName!="" || Session.fileName==null){
                        if(d<Duration(hours: 24*30)){                
                            PopMessage.info('Connection restored! Sending Screenshot');
                            await ScreenShotSender(fileName: Session.fileName!,recipient: Session.recipient!,subject: Session.subject!, message: Session.description!).sendOrSave();
                        }else{
                            await Session.reset();
                        }
                    }
                    if(Session.timeChanges!){
                        TimeChangeManager().sendOrSave();
                    }
                }
            }catch(e){}
        });
    }

    _initWatcher() {
        _controller ??= TimeChangeDetector.init;
        _subscription=_controller!.listen((event) {
                TimeChangeManager().sendOrSave();
            },
            onError: (error) => print('ERROR: $error'),
            onDone: () => print('STREAM_COMPLETE')
        );
    }
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Assignment',   
            theme: ThemeData.dark(),
            onGenerateRoute:router.generateRoute,
            initialRoute: AppRoutes.SplashScreen,
        );
    }

    @override
    void dispose() {
        super.dispose();
        _subscription.cancel();
    }
}