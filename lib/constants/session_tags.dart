class SessionTags{
    static const String IsTimeChanged='IsTimeChanged';
    static const String DocumentDir='DocumentDir';
    static const String FileName = 'FileName';
    static const String Recipient = 'Recipient';
    static const String Subject = 'Subject';
    static const String Description = 'Description';
    static const String CapturedDate = 'CapturedDate';
 }