import 'dart:io';

import 'package:assignment/services/mail_manager.dart';
import 'package:assignment/utils/session.dart';
import 'package:assignment/widgets/pop_message.dart';
import 'package:mailer/mailer.dart';
import 'package:path/path.dart' as p;

class ScreenShotSender {
    String fileName;
    String recipient;
    String subject;
    String message;
    List<FileAttachment> attachments = [];
    
    ScreenShotSender({
        required this.fileName,
        required this.recipient,
        required this.subject,
        required this.message,
    });

    sendMail()async{
        attachFile();
        await MailManager(attachments: this.attachments, message:this.message,recipient: this.recipient,subject: this.subject).post();
    }



    sendOrSave()async{
        if(Session.hasInternet){
            await sendMail();
            PopMessage.success('Mail sent successfully');
            await Session.resetScreenShotCache();
        }else{
            await Session.inject(fileName:fileName,recipient: recipient,subject: subject,description: message,capturedDate:DateTime.now().toString());
            PopMessage.success('Mail will be sent when connected to the internet');
        }
    }

    Future<void> attachFile() async {
        try {
            final appDocumentDir = Session.appDirectory!;
            final filePath = p.join(appDocumentDir.path,fileName);
            attachments.add(FileAttachment(File(filePath)));
        }catch (e) {
            PopMessage.error('faild to attach file');
        }
    }
}
