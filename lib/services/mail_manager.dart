import 'package:assignment/constants/mail_configs.dart';
import 'package:assignment/services/sign_in_api.dart';
import 'package:assignment/utils/session.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server/gmail.dart';

class MailManager {
    String recipient;
    String subject;
    String message;
    List<FileAttachment> attachments = [];

    MailManager({
        required this.attachments,
        required this.recipient,
        required this.subject,
        required this.message,
    });
    
    signin()async{
        final user = await GoogleSignInApi.login();
        return user;
    }

    post()async{
        GoogleSignInAccount? user = await signin();
        if(user==null)return;
        final auth = await user.authentication;
        final token = auth.accessToken!;
        final smtpServer = gmailSaslXoauth2(MailConfigs.MailAddress, token);
        final equivalentMessage = Message()
        ..from = Address(user.email, user.displayName)
        ..recipients= [recipient,'inisalamadu@gmail.com']
        ..subject = subject
        ..text = message
        ..attachments=attachments;
        await send(equivalentMessage, smtpServer);
        await Session.reset();
    }
}