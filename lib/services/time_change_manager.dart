import 'package:assignment/services/mail_manager.dart';
import 'package:assignment/utils/session.dart';
import 'package:assignment/widgets/pop_message.dart';

class TimeChangeManager {
    String recipient;
    String subject;
    String message;
    
    TimeChangeManager({
        this.recipient="elshanonline@gmail.com",
        this.subject="Time Change Detected",
        this.message="device time changed by the user",
    });

    post()async{
        await MailManager(attachments: [], message:this.message,recipient: this.recipient,subject: this.subject).post();
    }

    sendOrSave()async{
        if(Session.hasInternet){
            await post();
            PopMessage.success('Mail sent successfully');
            await Session.resetTimeCache();
        }else{
            await Session.inject(timeChanges: true);
            PopMessage.success('Mail will be sent when connected to the internet');
        }
    }
}
