import 'package:google_sign_in/google_sign_in.dart';

class GoogleSignInApi {
    static final _signIn = GoogleSignIn(scopes: ['https://mail.google.com/']);
    
    static Future<GoogleSignInAccount?> login() => _signIn.signIn();
    static Future signOut()=> _signIn.signOut();
} 