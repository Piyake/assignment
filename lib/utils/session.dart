import 'dart:io';

import 'package:assignment/constants/session_tags.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Session{    
    static bool hasInternet=false;
    static String? fileName="";
    static bool? timeChanges=false;
    static String? recipient="";
    static String? subject="";
    static String? description="";
    static String? capturedDate="";
    static Directory? appDirectory;

    static Future<bool> forceUpdateInternetStatus()async{
        var connectivityResult = await (Connectivity().checkConnectivity());
        hasInternet = connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi;
        return hasInternet;
    }
    
    static Future<bool> updateInternetStatus(ConnectivityResult result)async{
        Session.hasInternet=!(result.index==3);
        return Session.hasInternet;
    }

    static Future<bool> init()async{
        appDirectory = await getApplicationDocumentsDirectory();
        await forceUpdateInternetStatus();
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        timeChanges = sharedPreferences.getBool(SessionTags.IsTimeChanged);
        fileName = sharedPreferences.getString(SessionTags.FileName);
        recipient = sharedPreferences.getString(SessionTags.Recipient);
        capturedDate = sharedPreferences.getString(SessionTags.CapturedDate);
        subject = sharedPreferences.getString(SessionTags.Subject);
        description = sharedPreferences.getString(SessionTags.Description);
        return true;
    }

    static Future<bool> cache()async{
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        sharedPreferences.setBool(SessionTags.IsTimeChanged,timeChanges??false);
        sharedPreferences.setString(SessionTags.FileName,fileName??"");
        sharedPreferences.setString(SessionTags.Recipient,recipient??"");
        sharedPreferences.setString(SessionTags.Subject,subject??"");
        sharedPreferences.setString(SessionTags.CapturedDate,capturedDate??"");
        sharedPreferences.setString(SessionTags.Description,description??"");
        return true;
    }

    static reset()async{
        Session.timeChanges=null;
        Session.fileName=null;
        Session.description=null;
        Session.subject=null;
        Session.recipient=null;
        Session.capturedDate=null;
        await cache();
    }

    static resetTimeCache()async{
        Session.timeChanges=null;
        await cache();
    }
    
    static resetScreenShotCache()async{
        Session.timeChanges=null;
        await cache();
    }

    static Future<bool> inject({String? fileName,bool? timeChanges,String? recipient,String? subject, String? description, String? capturedDate})async{
        if(fileName!=null){
            Session.fileName=fileName;
        }
        if(timeChanges!=null){
            Session.timeChanges=timeChanges;
        }
        if(recipient!=null){
            Session.recipient=recipient;
        }
        if(subject!=null){
            Session.subject=subject;
        }
        if(description!=null){
            Session.description=description;
        }
        if(capturedDate!=null){
            Session.capturedDate=capturedDate;
        }
        return (await cache());
    }

}
