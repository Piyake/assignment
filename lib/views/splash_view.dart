import 'package:assignment/routes/route_constants.dart';
import 'package:assignment/utils/session.dart';
import 'package:flutter/material.dart';

class SplashView extends StatefulWidget {
  const SplashView({ Key? key }) : super(key: key);

  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
    @override
    void initState() {
        Future.delayed(Duration(seconds: 2),(){
            Navigator.pushReplacementNamed(context, AppRoutes.HomeView);
        });
        super.initState();
        initApp();
    }

    initApp()async{
        await Session.init();
    }

    @override
    Widget build(BuildContext context) {
        return SafeArea(
            child: Scaffold(
                body: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        Expanded(
                            child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.white
                                ),
                                padding: EdgeInsets.all(25),
                                child: Image.asset('assets/images/loading.gif'),
                            ),
                        ),
                    ],
                ),
            ),
        );
    }
}