import 'package:assignment/utils/session.dart';
import 'package:assignment/views/popups/mail_screenshot_popup.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class HomeView extends StatefulWidget {
  const HomeView({ Key? key }) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
    ScreenshotController screenshotController = ScreenshotController();
   
    @override
    Widget build(BuildContext context) {
        return Screenshot(
            controller: screenshotController,
            child: SafeArea(
                child: Scaffold(
                    appBar: AppBar(
                        title: Text('Assignment App'),
                        centerTitle: true,
                    ),
                    body: Container(
                        padding: EdgeInsets.all(12),
                        child: Column(
                            children: [
                                Expanded(
                                    child:Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                            Image.asset('assets/images/welcome.gif'),
                                            SizedBox(height: 10,),
                                            Text(
                                                'Hi All!',
                                                style: TextStyle(
                                                    fontSize: 24
                                                ),
                                            ),
                                            Text(
                                                'welcome to the application',
                                                style: TextStyle(
                                                    fontSize: 14
                                                ),
                                            )                                        ],
                                    )
                                ),
                                ElevatedButton(
                                    onPressed: ()async{
                                        String fileName = await takeAndSave();
                                        showDialog(
                                            useSafeArea: true,
                                            barrierColor: Theme.of(context).cardColor.withOpacity(0.7),
                                            context: context,
                                            builder: (BuildContext context) {
                                                return MailScreenShotPopup(
                                                    fileName: fileName
                                                );
                                            }
                                        );
                                    },
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                            Icon(
                                                Icons.screenshot,       
                                            ),
                                            SizedBox(width: 12,),
                                            Text(
                                                'Capture & Send'
                                            )
                                        ],
                                    )
                                )
                            ],
                        )
                    ),
                )
            ),
        );
    }

    Future<String> takeAndSave()async{
        final path = Session.appDirectory!.path;
        String fileName = DateTime.now().microsecondsSinceEpoch.toString()+'.jpeg';
        await screenshotController.captureAndSave(
            path,
            fileName:fileName,
        );
        return fileName;
    }
}