import 'dart:io';
import 'package:assignment/mixins/validation_mixin.dart';
import 'package:assignment/services/screenshot_manager.dart';
import 'package:assignment/utils/session.dart';
import 'package:assignment/widgets/field_lable.dart';
import 'package:assignment/widgets/popup_page_container.dart';
import 'package:assignment/widgets/textarea.dart';
import 'package:assignment/widgets/textbox.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;

class MailScreenShotPopup extends StatefulWidget {
    final String fileName;
    const MailScreenShotPopup({required this.fileName});

    @override
    State<MailScreenShotPopup> createState() => _MailScreenShotPopupState();
}

class _MailScreenShotPopupState extends State<MailScreenShotPopup>with ValidateMixin {
    TextEditingController mailTo =TextEditingController();
    TextEditingController subject =TextEditingController();
    TextEditingController message =TextEditingController();
    String? mailToError;
    String? subjectError;
    String? messageError;
    bool loading=false;
    @override

    Widget build(BuildContext context) {
        final appDocumentDir = Session.appDirectory!;
        final filePath = p.join(appDocumentDir.path,widget.fileName);
        File imageFile = File(filePath);
        return PopupPageContainer(
            loading: loading,
            title: 'Send Screenshot',
            child: Container(
                height: 350,
                child: SingleChildScrollView(
                    child: Column(
                        children: [
                            FieldLabel(label: 'Email',isMandatory: true),
                            TextBox(controller: mailTo, placeholder: 'mail to',errorText: mailToError,),
                            FieldLabel(label: 'Subject',isMandatory: true),
                            TextBox(controller: subject, placeholder: 'about the',errorText: subjectError),
                            FieldLabel(label: 'Message',isMandatory: true),
                            TextArea(controller: message, placeholder: 'say something',errorText: messageError),
                            FieldLabel(label: 'Screanshot Preview',isMandatory: true),
                            Container(
                                margin: EdgeInsets.only(top: 10),
                                padding: EdgeInsets.fromLTRB( 10, 0,10, 10),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey,width: 1),
                                    borderRadius: BorderRadius.circular(12)
                                ),
                                height: 200,
                                width: 120,
                                child: Image.file(imageFile)
                            )
                        ],
                    ),
                )
            ),
            actions: [
                TextButton(
                    child: Text('Cancel'),
                    onPressed:(){
                        Navigator.pop(context);
                    }, 
                ),
                TextButton(
                    child: Text('Send'),
                    onPressed:sendMail, 
                )
            ]
        );
    }

    sendMail()async{
        mailToError=mandatoryValidation(context, value: mailTo.text);
        subjectError=mandatoryValidation(context, value: subject.text);
        messageError=mandatoryValidation(context, value: message.text);
        setState(() { loading=true; });
        if(mailToError==null && subjectError==null && messageError==null){
            await ScreenShotSender(fileName: widget.fileName, recipient: mailTo.text, subject:subject.text, message: message.text).sendOrSave();
            setState(() { loading=false; });
            Navigator.pop(context);
        }else{
            setState(() { loading=false; });
        }
    }
}